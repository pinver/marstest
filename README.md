# marstest

## prerequisites

This is a webclient test application for the marservice package, so that should be installed and
running with the _startwars_ database.

`dub -- --sql-host=localhost --http-port=8082`

## install

The usual: `npm install`

## commands

`npm run build` - build the distribution in the _dist_ directory.
`npm start` - start webpack development server (with hot reload).

