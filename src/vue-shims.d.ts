
// It's automatically included by TypeScript.

import Vue from "vue";
import { ComponentOptions } from "vue";


// declare mars component options.
declare module 'vue/types/options' {
    interface ComponentOptions<V extends Vue> {
        mars?: any
    }
}

// instance property for mars.
declare module 'vue/types/vue' {
    interface Vue {
        $mars: any
    }
}

// Tells it that anything imported that ends in .vue has the same shape of the Vue constructor itself.
declare module "*.vue" {
    import Vue from "vue";
    export default Vue;
}

