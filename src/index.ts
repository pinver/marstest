
import Vue from 'vue';
import Vuex from 'vuex';
import { marsInstall, marsStore, theMarsClient } from 'marsclient';
import AppComponent from './pages/App.vue';

Vue.use(Vuex)
Vue.use(marsInstall, { 
    url: 'ws://'+window.location.hostname+':8082/ws',
});

const store = new Vuex.Store({
    state: {
        dummy: 0,
    },
    modules: {
        mars: marsStore
    },
});



let v = new Vue({
    el: '#app',
    store,
    template: '<app-component/>',
    components: { AppComponent },
    created: function () {
        // ... connect to the server, install the application store ...
        theMarsClient.initializeCommunication(store, function() {});
    }
})

